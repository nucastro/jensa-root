#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include "id.h"
#include "hits_t.h"
#include "detectors/detector_t.h"
#include "detectors/detectors.h"

int main (int argc, char *argv[]) {
  std::vector<detector_t*> allDets;
  std::map<daq_id_t, det_id_t> chanMap;
  FILE* detfile;

  if ((detfile=fopen("dets.cfg", "r")) == NULL) {
    fprintf(stderr,"ERROR: Can't open config file '%s'!\n", "dets.cfg");
    exit(-2);
  }
  while (!feof(detfile)) {
    char type[100];
    fscanf(detfile, "%s", type);

    if(feof(detfile)) {
      break;
    }

    if (strcmp(type, "BB15_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB15_F_t(name, id));
    } else if (strcmp(type, "BB15_B") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB15_B_t(name, id));
    } else if (strcmp(type, "BB10_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new BB10_F_t(name, id));
    } else if (strcmp(type, "YY1_F") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new YY1_F_t(name, id));
    } else if (strcmp(type, "PSIC_XY") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new PSIC_XY_t(name, id));
    } else if (strcmp(type, "PSIC_E") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new PSIC_E_t(name, id));
    } else if (strcmp(type, "HAGRID") == 0) {
      char name[100];
      daq_id_t id;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld", name, &id.sourceid, &id.crateid, &id.slotid, &id.chanid);
      allDets.push_back(new HAGRID_t(name, id));
    } else if (strcmp(type, "HABA_QUAD") == 0) {
      char name[100];
      daq_id_t id1;
      daq_id_t id2;
      fscanf(detfile, "%s\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld", name, &id1.sourceid, &id1.crateid, &id1.slotid, &id1.chanid, &id2.sourceid, &id2.crateid, &id2.slotid, &id2.chanid);
      allDets.push_back(new HABA_QUAD_t(name, id1, id2));
    }
  }

  for (size_t i = 0; i < allDets.size(); ++i) {
    for (size_t j = 0; j < allDets[i]->size(); ++j) {
      daq_id_t id = allDets[i]->det_to_daq(j);
      det_id_t det(i, j);

      if (chanMap.count(id) != 0) {
        fprintf(stderr, "daq_id (%ld,%ld,%ld,%ld) already used.\n", id.sourceid, id.crateid, id.slotid, id.chanid);
      }
      chanMap[id] = det;
    }
  }

  TFile* file = new TFile("out.root", "update");
  TTree* tree = (TTree*)file->Get("tree");

  hits_t hits(1000);//FIXME: make adjustable

  tree->SetBranchAddress("size",     (ULong64_t*)hits.get_size_ptr());
  tree->SetBranchAddress("sourceid", (ULong64_t*)hits.get_sourceid_ptr());
  tree->SetBranchAddress("crateid",  (ULong64_t*)hits.get_crateid_ptr());
  tree->SetBranchAddress("slotid",   (ULong64_t*)hits.get_slotid_ptr());
  tree->SetBranchAddress("chanid",   (ULong64_t*)hits.get_chanid_ptr());
  tree->SetBranchAddress("rawval",   (ULong64_t*)hits.get_rawval_ptr());
 //FIXME setbranchaddress if branches already exist
  tree->Branch("detid", hits.get_detid_ptr(), "detid[size]/L");
  tree->Branch("detch", hits.get_detch_ptr(), "detch[size]/l");
  tree->Branch("value", hits.get_value_ptr(), "value[size]/l");

  int nEntries = tree->GetEntries();
  for (int i = 0; i < nEntries; ++i) {
    printf("%5.2f%%\r", 100.*i/nEntries);
    tree->GetEntry(i);

    uint64_t size = *hits.get_size_ptr();
    uint64_t*  so = hits.get_sourceid_ptr();
    uint64_t*  cr = hits.get_crateid_ptr();
    uint64_t*  sl = hits.get_slotid_ptr();
    uint64_t*  ch = hits.get_chanid_ptr();
    uint64_t*  rv = hits.get_rawval_ptr();
    int64_t*   di = hits.get_detid_ptr();
    uint64_t*  dc = hits.get_detch_ptr();
    uint64_t*   v = hits.get_value_ptr();

    for (size_t j = 0; j < size; ++j) {
      daq_id_t id1(so[j], cr[j], sl[j], ch[j]);

      if (chanMap.count(id1) != 0) {
        det_id_t id2(chanMap[id1]);

        di[j] = id2.detid;
        dc[j] = id2.chid;
        v[j] = allDets[id2.detid]->val_corr(id1, rv[j]);
      } else {
        di[j] = -1;
        dc[j] = 0;
        v[j] = rv[j];
      }
    }
    tree->Fill();

    for (size_t j = 0; j < 1000; ++j) { //FIXME
        di[j] = 0;
        dc[j] = 0;
    }
  }
  file->Write(0, TObject::kOverwrite);
  file->Close();
}
