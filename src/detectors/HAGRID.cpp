#include <string>
#include <stdint.h>
#include "HAGRID.h"
#include "../id.h"

HAGRID_t::HAGRID_t(std::string n, daq_id_t id): detector_t(9) {
  name = n;
  startId = id;
}

bool HAGRID_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if (id.slotid == startId.slotid) {
        if ((id.chanid >= startId.chanid) && (id.chanid < startId.chanid + numChans)) {
          return true;
        }
      }
    }
  }
  return false;
}

uint64_t HAGRID_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return id.chanid-startId.chanid;
}

daq_id_t HAGRID_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.chanid += ch;
  return id;
}
