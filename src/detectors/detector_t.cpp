#include <string>
#include <stdint.h>
#include "detector_t.h"
#include "../id.h"

detector_t::detector_t(size_t c) {
  numChans = c;
}

std::string detector_t::get_name() {
  return name;
}

size_t detector_t::size() {
  return numChans;
}

uint64_t detector_t::val_corr(daq_id_t id __attribute__((unused)), uint64_t value) {
  return value;
}
