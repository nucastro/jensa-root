#ifndef _BB10_H_
#define _BB10_H_

#include <string>
#include <stdint.h>
#include "detector_t.h"
#include "../id.h"

class BB10_F_t: public detector_t {
protected:
  daq_id_t startId;
public:
  BB10_F_t(std::string n, daq_id_t id);
public:
  virtual uint64_t val_corr(daq_id_t id, uint64_t value);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};

#endif
