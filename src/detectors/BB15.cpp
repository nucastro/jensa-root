#include <string>
#include <stdint.h>
#include "BB15.h"
#include "../id.h"

BB15_F_t::BB15_F_t(std::string n, daq_id_t id): detector_t(64) {
  name = n;
  startId = id;
}

uint64_t BB15_F_t::val_corr(daq_id_t id __attribute__((unused)), uint64_t value) {
  return 16383-value;
}

bool BB15_F_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if ((id.slotid >= startId.slotid) && (id.slotid < startId.slotid + 4)) {
        return true;
      }
    }
  }
  return false;
}

uint64_t BB15_F_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  if ((id.slotid - startId.slotid)%2 == 0) {
    return 16*(id.slotid-startId.slotid)+id.chanid;
  } else {
    return 16*(id.slotid-startId.slotid)+(15-id.chanid);
  }
}

daq_id_t BB15_F_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.slotid += ch/16;
  id.chanid = ch%16;
  if ((ch/16)%2 == 1) {
    id.chanid = 15 - id.chanid;
  }
  return id;
}

/*
 *
 */
BB15_B_t::BB15_B_t(std::string n, daq_id_t id): detector_t(4) {
  name = n;
  startId = id;
}

bool BB15_B_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if (id.slotid == startId.slotid) {
        if ((id.chanid >= startId.chanid) && (id.chanid < startId.chanid + numChans)) {
          return true;
        }
      }
    }
  }
  return false;
}

uint64_t BB15_B_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return id.chanid - startId.chanid;
}

daq_id_t BB15_B_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.chanid += ch;
  return id;
}
