#ifndef _PSIC_H_
#define _PSIC_H_

#include <string>
#include <stdint.h>
#include "detector_t.h"
#include "../id.h"

class PSIC_XY_t: public detector_t {
protected:
  daq_id_t startId;
public:
  PSIC_XY_t(std::string n, daq_id_t id);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};

class PSIC_E_t: public detector_t {
protected:
  daq_id_t startId;
public:
  PSIC_E_t(std::string n, daq_id_t id);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
};

#endif
