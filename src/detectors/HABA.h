#ifndef _HABA_H_
#define _HABA_H_

#include <string>
#include <stdint.h>
#include "detector_t.h"
#include "../id.h"

class HABA_QUAD_t: public detector_t {
protected:
  daq_id_t startId1;
  daq_id_t startId2;
public:
  HABA_QUAD_t(std::string n, daq_id_t id1, daq_id_t id2);
public:
  virtual bool contains_daq(daq_id_t id);
  virtual uint64_t daq_to_det(daq_id_t id);
  virtual daq_id_t det_to_daq(uint64_t ch);
protected:
  virtual bool contains_daq1(daq_id_t id);
  virtual bool contains_daq2(daq_id_t id);
};
#endif
