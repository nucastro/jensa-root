#include <string>
#include <stdint.h>
#include "HABA.h"
#include "../id.h"

HABA_QUAD_t::HABA_QUAD_t(std::string n, daq_id_t id1, daq_id_t id2): detector_t(20) {
  name = n;
  startId1 = id1;
  startId2 = id2;
}

bool HABA_QUAD_t::contains_daq1(daq_id_t id) {
  if (id.sourceid == startId1.sourceid) {
    if (id.crateid == startId1.crateid) {
      if (id.slotid == startId1.slotid) {
        return true;
      }
    }
  }
  return false;
}

bool HABA_QUAD_t::contains_daq2(daq_id_t id) {
  if (id.sourceid == startId2.sourceid) {
    if (id.crateid == startId2.crateid) {
      if (id.slotid == startId2.slotid) {
        if ((id.chanid >= startId2.chanid) && (id.chanid < startId2.chanid + 4)) {
          return true;
        }
      }
    }
  }
  return false;
}

bool HABA_QUAD_t::contains_daq(daq_id_t id) {
  return (contains_daq1(id) || contains_daq2(id));
}

uint64_t HABA_QUAD_t::daq_to_det(daq_id_t id) {
  if (contains_daq1(id)) {
    return id.chanid-startId1.chanid;
  }
  if (contains_daq2(id)) {
    return id.chanid-startId2.chanid;
  }
  return -1;
}

daq_id_t HABA_QUAD_t::det_to_daq(uint64_t ch) {
  if (ch < 16) {
    daq_id_t id = startId1;
    id.chanid += ch;
    return id;
  } else {
    daq_id_t id = startId2;
    id.chanid += ch%16;
    return id;
  }
}
