#include <string>
#include <stdint.h>
#include "PSIC.h"
#include "../id.h"

PSIC_XY_t::PSIC_XY_t(std::string n, daq_id_t id): detector_t(32) {
  name = n;
  startId = id;
}

bool PSIC_XY_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if ((id.slotid >= startId.slotid) && (id.slotid < startId.slotid + 2)) {
        return true;
      }
    }
  }
  return false;
}

uint64_t PSIC_XY_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return 16*(id.slotid-startId.slotid)+id.chanid;
}

daq_id_t PSIC_XY_t::det_to_daq(uint64_t ch) {
  daq_id_t id = startId;
  id.slotid += ch/16;
  id.chanid += ch%16;
  return id;
}

/*
 *
 */
PSIC_E_t::PSIC_E_t(std::string n, daq_id_t id): detector_t(1) {
  name = n;
  startId = id;
}

bool PSIC_E_t::contains_daq(daq_id_t id) {
  if (id.sourceid == startId.sourceid) {
    if (id.crateid == startId.crateid) {
      if (id.slotid == startId.slotid) {
        if (id.chanid == startId.chanid) {
          return true;
        }
      }
    }
  }
  return false;
}

uint64_t PSIC_E_t::daq_to_det(daq_id_t id) {
  if (!contains_daq(id)) {
    return -1;
  }
  return 0;
}

daq_id_t PSIC_E_t::det_to_daq(uint64_t ch __attribute__((unused))) {
  daq_id_t id = startId;
  return id;
}
