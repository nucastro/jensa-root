#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include "id.h"
#include "hits_t.h"
#include "cal_info_t.h"

int main (int argc, char *argv[]) {
  std::map<daq_id_t, cal_info_t> calMap;
  FILE* calfile;

  if ((calfile=fopen("calib.dat", "r")) == NULL) {
    fprintf(stderr,"ERROR: Can't open calibration file '%s'!\n", "calib.dat");
    exit(-2);
  }
  while (!feof(calfile)) {
    daq_id_t id;
    cal_info_t cal;
    // Read line
    fscanf(calfile, "%ld\t%ld\t%ld\t%ld\t%lf\t%lf", &id.sourceid, &id.crateid, &id.slotid, &id.chanid, &cal.offset, &cal.slope);

    if(feof(calfile)) {
      break;
    }
    // Process data
    if (calMap.count(id) != 0) {
      fprintf(stderr, "daq_id (%ld,%ld,%ld,%ld) already has a calibration.\n", id.sourceid, id.crateid, id.slotid, id.chanid);
    }
    calMap[id] = cal;
  }

  TFile* file = new TFile("out.root", "update");
  TTree* tree = (TTree*)file->Get("tree");

  hits_t hits(1000);//FIXME: make adjustable

  tree->SetBranchAddress("size",     (ULong64_t*)hits.get_size_ptr());
  tree->SetBranchAddress("sourceid", (ULong64_t*)hits.get_sourceid_ptr());
  tree->SetBranchAddress("crateid",  (ULong64_t*)hits.get_crateid_ptr());
  tree->SetBranchAddress("slotid",   (ULong64_t*)hits.get_slotid_ptr());
  tree->SetBranchAddress("chanid",   (ULong64_t*)hits.get_chanid_ptr());
  tree->SetBranchAddress("value",    (ULong64_t*)hits.get_value_ptr());
 //FIXME setbranchaddress if branches already exist
  tree->Branch("energy", hits.get_energy_ptr(), "energy[size]/D");

  int nEntries = tree->GetEntries();
  for (int i = 0; i < nEntries; ++i) {
    printf("%5.2f%%\r", 100.*i/nEntries);
    tree->GetEntry(i);

    uint64_t size = *hits.get_size_ptr();
    uint64_t*  so = hits.get_sourceid_ptr();
    uint64_t*  cr = hits.get_crateid_ptr();
    uint64_t*  sl = hits.get_slotid_ptr();
    uint64_t*  ch = hits.get_chanid_ptr();
    uint64_t*   v = hits.get_value_ptr();
    double*    en = hits.get_energy_ptr();

    for (size_t j = 0; j < size; ++j) {
      daq_id_t id(so[j], cr[j], sl[j], ch[j]);

      if (calMap.count(id) != 0) {
        cal_info_t cal(calMap[id]);

        en[j] = cal.offset + cal.slope * v[j];
      } else {
        en[j] = v[j];
      }
    }
    tree->Fill();

    for (size_t j = 0; j < 1000; ++j) { //FIXME
        en[j] = 0;
    }
  }
  file->Write(0, TObject::kOverwrite);
  file->Close();
}
