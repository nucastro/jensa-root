#include "hits_t.h"
#include <stdint.h>
#include <vector>

hits_t::hits_t() {
  maxMult = 0;
  reserve(1);
  Reset();
}

hits_t::hits_t(uint64_t s) {
  maxMult = 0;
  reserve(s);
  Reset();
}

void hits_t::Reset() {
  for (size_t i = 0; i < maxMult; ++i) {
    sourceid[i] = 0;
    crateid[i]  = 0;
    slotid[i]   = 0;
    chanid[i]   = 0;
    rawval[i]   = 0;
    time[i]     = 0;
    detid[i]    = 0;
    detch[i]    = 0;
    value[i]    = 0;
    energy[i]   = 0;
  }
  size=0;
}

void hits_t::reserve(size_t s) {
  if (s > maxMult) {
    maxMult = s;
    sourceid.reserve(s);
    slotid.reserve(s);
    crateid.reserve(s);
    chanid.reserve(s);
    rawval.reserve(s);
    time.reserve(s);
    detid.reserve(s);
    detch.reserve(s);
    value.reserve(s);
    energy.reserve(s);
  }
}

size_t hits_t::get_maxMult() {
  return maxMult;
}

uint64_t* hits_t::get_size_ptr() {
  return &size;
}

uint64_t* hits_t::get_sourceid_ptr() {
  return sourceid.data();
}

uint64_t* hits_t::get_crateid_ptr() {
  return crateid.data();
}

uint64_t* hits_t::get_slotid_ptr() {
  return slotid.data();
}

uint64_t* hits_t::get_chanid_ptr() {
  return chanid.data();
}

uint64_t* hits_t::get_rawval_ptr() {
  return rawval.data();
}

double* hits_t::get_time_ptr() {
  return time.data();
}

int64_t* hits_t::get_detid_ptr() {
  return detid.data();
}

uint64_t* hits_t::get_detch_ptr() {
  return detch.data();
}

uint64_t* hits_t::get_value_ptr() {
  return value.data();
}

double* hits_t::get_energy_ptr() {
  return energy.data();
}
