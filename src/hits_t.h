#ifndef _HITS_T_H_
#define _HITS_T_H_

#include <stdint.h>
#include <cstddef>
#include <vector>

class hits_t {
protected:
  size_t maxMult;
  uint64_t size;
  std::vector<uint64_t> sourceid;
  std::vector<uint64_t> slotid;
  std::vector<uint64_t> crateid;
  std::vector<uint64_t> chanid;
  std::vector<uint64_t> rawval;
  std::vector<double>   time;
  std::vector<int64_t>  detid;
  std::vector<uint64_t> detch;
  std::vector<uint64_t> value;
  std::vector<double> energy;
public:
  hits_t();
  hits_t(size_t s);
public:
  void Reset();
  void reserve(size_t s);
public:
  size_t get_maxMult();
  uint64_t* get_size_ptr();
  uint64_t* get_sourceid_ptr();
  uint64_t* get_crateid_ptr();
  uint64_t* get_slotid_ptr();
  uint64_t* get_chanid_ptr();
  uint64_t* get_rawval_ptr();
  double*   get_time_ptr();
  int64_t*  get_detid_ptr();
  uint64_t* get_detch_ptr();
  uint64_t* get_value_ptr();
  double*   get_energy_ptr();
};

#endif
